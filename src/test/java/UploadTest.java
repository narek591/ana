import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.TrailInnerPage;
import pages.upload.UploadPage;
import pages.upload.UploadPageDelete;
import pages.upload.UploadPageEdit;
import utils.DriverFactory;
import utils.Helper;
import utils.WaitHelper;
import utils.assertHelpers.HardAssertHelper;
import utils.assertHelpers.SoftAssertHelper;

public class UploadTest extends BaseTest {
    final String filePath = "\\src\\main\\resources\\Khustup\\khustup_trail.gpx";
    final String trailYoutubeURI = "https://www.youtube.com/watch?v=Rwe5FKUI5SQ";
    final String trailEditedName = "Khustup Route To the Top: " + Helper.getTodayDate();
    String trailName = "Khustup Route";
    String externalAssetID;
    String uploadedTrailId;

    @BeforeMethod
    void initUploadTest() {
        setCookie(BasePage.ABOUT_US_PATH);
    }

    @Test()
    void trailAdd() {
        SoftAssertHelper softAssert = new SoftAssertHelper();

        DriverFactory.getDriver().get(BasePage.BASE_URL + UploadPage.PATH);
        UploadPage uploadPage = new UploadPage();

        String rootPath = new java.io.File("").getAbsolutePath();
        String fileFullPath = rootPath + filePath;

        uploadPage.fileInput.sendKeys(fileFullPath);
        HardAssertHelper.getTextContains(uploadPage.uploadedFileTitle, "Khustup Walk", "Wrong file uploaded");

        uploadPage.addGpsContinueBtn.click();
        uploadedTrailId = Helper.getParameterFromURI("id", DriverFactory.getDriver().getCurrentUrl());

        if (uploadedTrailId.equals("")) {
            HardAssertHelper.manualError("Parameter ID not found");
        }

        sendKeysAndAssert(softAssert, uploadPage.fileName, trailName, "Trail Name was inputted wrong");
        sendKeysAndAssert(softAssert, uploadPage.description, "Some Short Description for Route", "Trail Name was inputted wrong");

        uploadPage.skillDifficult.click();
        softAssert.classAttrContains(uploadPage.skillDifficult, "active", "Skill not set properly");

        uploadPage.uploadExternalBtn.click();
        WaitHelper.waitUntilIsVisibility(uploadPage.uploadExternalInput);
        sendKeysAndAssert(uploadPage.uploadExternalInput, trailYoutubeURI, "Youtube URI was inputted wrong");
        uploadPage.uploadExternalSave.click();

        String externalAssetXpath =
                uploadPage.getPreviewImageXpath(
                        Helper.getParameterFromURI("v", trailYoutubeURI)
                );
        WaitHelper.waitForExist(externalAssetXpath);
        externalAssetID = DriverFactory.getDriver().findElement(
                By.xpath(externalAssetXpath)
        ).getAttribute("data-id");

        if (externalAssetID.equals("")) {
            HardAssertHelper.manualError("External asset ID not found");
        }

        WaitHelper.waitUntilIsClickable(uploadPage.finalContinueBtn);
        uploadPage.finalContinueBtn.click();
        uploadPage.doneAlone.click();
        softAssert.assertAll();
    }

    @Test(dependsOnMethods = "trailAdd", priority = 1)
    void trailEdit() {
        SoftAssertHelper softAssert = new SoftAssertHelper();

        String FullURL = BasePage.BASE_URL + TrailInnerPage.innerViewPATH(trailName, uploadedTrailId);
        DriverFactory.getDriver().get(FullURL);

        UploadPageEdit uploadPageEdit = new UploadPageEdit();
        TrailInnerPage trailInnerPage = new TrailInnerPage();

        trailInnerPage.editBtn.click();

        sendKeysAndAssert(softAssert, uploadPageEdit.fileName, trailEditedName, "File name typed wrong");
        trailName = trailEditedName;

        sendKeysAndAssert(softAssert, uploadPageEdit.description, "Some Short Description for Route with additional info", "Description typed wrong");

        uploadPageEdit.skillModerate.click();
        softAssert.classAttrContains(uploadPageEdit.skillModerate, "active", "Skill not set properly");

        uploadPageEdit.privacyBtn.click();
        uploadPageEdit.privateOption.click();
        uploadPageEdit.privateModalSubmitBtn.click();
        uploadPageEdit.privacyModalToPrivateConfirmBtn.click();
        softAssert.getTextEquals(uploadPageEdit.privacyStatus, "Private", "Privacy change failed");

        sendKeysAndAssert(softAssert, uploadPageEdit.tags, "Khustup Mountain Armenia Kapan", "Tags typed wrong");
        sendKeysAndAssert(softAssert, uploadPageEdit.externalURL, "https://khustup.am/about-us", "External URL typed wrong");

        softAssert.assertAll();
        uploadPageEdit.finalContinueBtn.click();
    }

    @Test(dependsOnMethods = "trailAdd", priority = 2)
    void trailDelete() {
        final String FullURL = BasePage.BASE_URL + TrailInnerPage.innerViewPATH(trailName, uploadedTrailId);
        DriverFactory.getDriver().get(FullURL);

        UploadPageDelete uploadPageDelete = new UploadPageDelete();
        TrailInnerPage trailInnerPage = new TrailInnerPage();

        HardAssertHelper.getTextContains(trailInnerPage.titleText, trailName, "Wrong Trail opened");

        trailInnerPage.editBtn.click();
        HardAssertHelper.textContains(Helper.getPathFromURI(), "edit.do", "Not edit mode");

        uploadPageDelete.deleteBtn.click();
        DriverFactory.acceptAlert();

        DriverFactory.getDriver().get(FullURL);
        HardAssertHelper.getTextContains(uploadPageDelete.error404text, "Page not found", "Not deleted properly");
    }
}