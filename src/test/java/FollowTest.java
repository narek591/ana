import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.FollowPage;
import utils.DriverFactory;
import utils.WaitHelper;
import utils.assertHelpers.HardAssertHelper;

public class FollowTest extends BaseTest {

    FollowPage followPage;

    @BeforeMethod
    void find() {
        setCookie(BasePage.ABOUT_US_PATH);
        DriverFactory.getDriver().get(BasePage.BASE_URL + FollowPage.PATH);
        followPage = new FollowPage();
    }

    @Test
    void follow() {
        HardAssertHelper.getTextContains(followPage.followButton, "Follow", "Follow button text is not Follow");
        followPage.followButton.click();
        WaitHelper.waitUntilAttributeContains(followPage.followButton, "title", "Following");
    }

    @AfterMethod
    void unfollow() {
        HardAssertHelper.getTextContains(followPage.followButton, "Following", "Following button text is not Follow");
        followPage.followButton.click();
        WaitHelper.waitUntilAttributeContains(followPage.followButton, "title", "Follow");
    }

}