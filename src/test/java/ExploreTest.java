import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.ExplorePage;
import utils.DriverFactory;

public class ExploreTest extends BaseTest {
    ExplorePage explorePage;

    @BeforeMethod
    void find() {
        setCookie(BasePage.BASE_URL);
        DriverFactory.getDriver().get(BasePage.BASE_URL + ExplorePage.PATH);
        explorePage = new ExplorePage();
    }

    @Test
    void activitiesFilter() {
        explorePage.activities.click();
        explorePage.hiking.click();
        explorePage.applyActivitiesFilterBtn.click();
    }

    @Test
    void distanceFilter() {
        explorePage.distance.click();
        explorePage.lineFirstBtn.click();

    }

    @Test
    void moreFilters() {
        explorePage.moreFilters.click();
    }
}