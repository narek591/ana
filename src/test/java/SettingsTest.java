import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.SettingsPage;
import utils.DriverFactory;
import utils.assertHelpers.SoftAssertHelper;

public class SettingsTest extends BaseTest {

    SettingsPage settingsPage;

    @BeforeMethod
    void find() {
        setCookie(BasePage.ABOUT_US_PATH);
        DriverFactory.getDriver().get(BasePage.BASE_URL + SettingsPage.PATH);
        settingsPage = new SettingsPage();
    }

    @Test
    void settings() {
        SoftAssertHelper softAssert = new SoftAssertHelper();
        softAssert.getTextEquals(settingsPage.username, "iamcrazy", "Should be iamcrazy" );
        softAssert.getTextEquals(settingsPage.emailAddress, "narek.ntm@gmail.com", "Should be narek.ntm@gmail.com");
        sendKeysAndAssert(softAssert, settingsPage.web, "https://www.wikiloc.com/wikiloc/showProfile.do", "Web was inputted wrong");settingsPage.web.clear();
        sendKeysAndAssert(softAssert,settingsPage.aboutYou,"Hello", "About you was  wrong");
        settingsPage.activities.click();
        settingsPage.hiking.click();
        settingsPage.select.click();
        settingsPage.unitsOfMeasurement.click();
        settingsPage.metric.click();
        softAssert.assertAll();
        settingsPage.save.click();
    }

    @AfterMethod
    void deleteActivity() {
        settingsPage.activities.click();
        settingsPage.hiking.click();
        settingsPage.select.click();
        settingsPage.save.click();
    }
}

