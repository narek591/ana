import org.openqa.selenium.Alert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.PromoteBusinessPage;
import utils.DriverFactory;
import utils.assertHelpers.SoftAssertHelper;

public class PromoteBusinessTest extends BaseTest {
    PromoteBusinessPage promoteBusinessPage;

    @BeforeMethod
    void find() {
        setCookie(BasePage.ABOUT_US_PATH);
        DriverFactory.getDriver().get(BasePage.BASE_URL + PromoteBusinessPage.PATH);
        promoteBusinessPage = new PromoteBusinessPage();
    }

    @Test
    void businessCreation() {
        SoftAssertHelper softAssert = new SoftAssertHelper();
        sendKeysAndAssert(softAssert, promoteBusinessPage.nameOfYourBusiness, "Do your best", "Name of your business was inputted wrong");
        promoteBusinessPage.options.click();
        promoteBusinessPage.type.click();
        sendKeysAndAssert(softAssert, promoteBusinessPage.address, "Armenia, Yerevan", "Address was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.email, "smt@gmail.com", "Email was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.webSite, "https://www.wikiloc.com/", "WebSite was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.reservationUrl, "https://www.wikiloc.com/wikiloc/user.do?id=9076836", "Reservation URL was inputted wrong ");
        softAssert.assertAll();
        promoteBusinessPage.submit.click();

        // publishBusiness
        sendKeysAndAssert(softAssert, promoteBusinessPage.taxidentificationnumber, "12345678", "Tax identification number was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.companyName, "Do your best", "Name of your business was inputted wrong");
        promoteBusinessPage.countryOption.click();
        promoteBusinessPage.country.click();
        sendKeysAndAssert(softAssert, promoteBusinessPage.businessaddress, "Armenia", "Address of your business was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.postalCode, "0064", "Postal Code of your business was inputted wrong");
        promoteBusinessPage.countryList.click();
        promoteBusinessPage.countryCode.click();
        sendKeysAndAssert(softAssert, promoteBusinessPage.number, "77777777", "Phone number of your business was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.firstName, "Ana", "First name of your business was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.lastName, "Automation", "Last name of your business was inputted wrong");
        promoteBusinessPage.firstName.sendKeys("ANA");
        sendKeysAndAssert(softAssert, promoteBusinessPage.personalEmail, "smt@gmail.com", "Email of your business was inputted wrong");
        softAssert.assertAll();
        promoteBusinessPage.submitbusiness.click();

        //card's info
        sendKeysAndAssert(softAssert, promoteBusinessPage.cardNumber, "1234567891011214", "Card number was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.expDate1, "12", "Expiration date1 was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.expDate2, "24", "Expiration date2 was inputted wrong");
        sendKeysAndAssert(softAssert, promoteBusinessPage.securityCode, "444", "Security code was inputted wrong");
        softAssert.assertAll();
        promoteBusinessPage.accept.click();

        Alert alert = DriverFactory.getDriver().switchTo().alert();
        String alertMessage = DriverFactory.getDriver().switchTo().alert().getText(); // capture alert message
        System.out.println(alertMessage); // Print Alert Message
        alert.accept();


    }
}
