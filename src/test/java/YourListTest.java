import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.YourListPage;
import utils.DriverFactory;
import utils.WaitHelper;

public class YourListTest extends BaseTest {
    YourListPage yourListPage;

    @BeforeMethod
    void find() {
        setCookie(BasePage.ABOUT_US_PATH);
        DriverFactory.getDriver().get(BasePage.BASE_URL + YourListPage.PATH);
        yourListPage = new YourListPage();
    }

    @Test
    void favorites() {
        yourListPage.road.click();
        yourListPage.saveToList.click();
        WaitHelper.waitUntilIsClickable(yourListPage.favorites);
        yourListPage.favorites.click();
        yourListPage.close.click();
    }

    @Test
    void wantToGo() {
        yourListPage.road1.click();
        yourListPage.saveToList.click();
        WaitHelper.waitUntilIsClickable(yourListPage.wantToGo);
        yourListPage.wantToGo.click();
        yourListPage.close.click();
    }

    @Test
    void clap() {
        yourListPage.road.click();
        yourListPage.clap.click();
    }

    @Test
    void comment() {
        yourListPage.road.click();
        yourListPage.comment.click();
        yourListPage.comment.sendKeys("Cool");
        yourListPage.sendComment.click();
    }
}
