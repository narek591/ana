import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.BasePage;
import utils.Browser;
import utils.DriverFactory;
import utils.assertHelpers.HardAssertHelper;
import utils.assertHelpers.SoftAssertHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BaseTest {
    public static String loginToken = "6c22475ad65211eba5f62368bf85e3c1#3f059f0f4196d2ac04e979e8587aaabb";

    @BeforeMethod
    void setup() {
        DriverFactory.initDriver(Browser.CHROME);
    }

    @AfterMethod
    void tearDown() {
        DriverFactory.quitDriver();
    }

    public static void setCookie(String URI) {
        /*
         * Get Date of now
         * Add one day
         * Convert to right format
         * */
        DateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String newDate = sdf.format(calendar.getTime());
        try {
            date = sdf.parse(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /*
         * Create Cookie with provided loginToken and generated date
         * */
        Cookie cookie = new Cookie(
                "wluseruuid",
                loginToken,
                ".wikiloc.com",
                "/",
                date,
                true);

        /*
         * Get URI passed to method
         * Add Cookie
         * Refresh Page to update Cookies
         * */
        DriverFactory.getDriver().get(URI);
        DriverFactory.getDriver().manage().addCookie(cookie);
        DriverFactory.getDriver().navigate().refresh();
    }

    public void sendKeysAndAssert(WebElement element, String value, String message){
        BasePage.cleanAndSendKeys(element, value);
        HardAssertHelper.valueAttrEquals(element, value, message);
    }

    public void sendKeysAndAssert(SoftAssertHelper softAssert, WebElement element, String value, String message){
        BasePage.cleanAndSendKeys(element, value);
        softAssert.valueAttrEquals(element, value, message);
    }
}