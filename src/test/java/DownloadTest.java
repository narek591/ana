import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.DownloadPage;
import utils.DriverFactory;
import utils.WaitHelper;

public class DownloadTest extends BaseTest {
    DownloadPage downloadPage;

    @BeforeMethod
    void find() {
        setCookie(BasePage.BASE_URL);
        DriverFactory.getDriver().get(BasePage.BASE_URL + DownloadPage.PATH);
        downloadPage = new DownloadPage();
    }

    @Test
    void trailDownload() {
        downloadPage.train.click();
        downloadPage.trainTrail.click();
        downloadPage.download.click();
        downloadPage.googleEarth.click();
        WaitHelper.waitUntilIsVisibility(downloadPage.findDownload);
        downloadPage.findDownload.click();
        WaitHelper.waitUntilIsVisibility(downloadPage.downloadCompleted);

    }
}