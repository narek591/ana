package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverFactory {
    static WebDriver driver;

    public static WebDriver getDriver() {
        driver.manage().window().maximize();
        return driver;
    }

    public static void initDriver(Browser browser) {
        switch (browser) {
            case CHROME: {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            }
            case FIREFOX: {
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            }
            case EDGE: {
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;
            }
            case EXPLORER: {
                WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
            }
            default:
                System.out.println(browser + " browser not found");
        }
    }

    public static void acceptAlert(){
        getDriver().switchTo().alert().accept();
    }

    public static void dismissAlert(){
        getDriver().switchTo().alert().dismiss();
    }

    public static void quitDriver() {
        driver.quit();
    }

}
