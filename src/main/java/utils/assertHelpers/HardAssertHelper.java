package utils.assertHelpers;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class HardAssertHelper {

    public static void valueAttrEquals(WebElement element, String expectedValue, String message) {
        String actualValue = element.getAttribute("value");
        Assert.assertEquals(actualValue, expectedValue, message);
    }

    public static void getTextContains(WebElement element, String expectedValue, String message) {
        String actualValue = element.getText();
        Assert.assertTrue(actualValue.contains(expectedValue), message);
    }

    public static void classAttrContains(WebElement element, String expectedValue, String message) {
        String actualValue = element.getAttribute("class");
        Assert.assertTrue(actualValue.contains(expectedValue), message);
    }

    public static void textContains(String actualValue, String expectedValue, String message) {
        Assert.assertTrue(actualValue.contains(expectedValue), message);
    }

    public static void manualError(String message) {
        Assert.fail(message);
    }
}
