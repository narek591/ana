package utils.assertHelpers;

import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

public class SoftAssertHelper {
    SoftAssert instance;

    public SoftAssertHelper() {
        instance = new SoftAssert();
    }

    public void valueAttrEquals(WebElement element, String expectedValue, String message) {
        String actualValue = element.getAttribute("value");
        instance.assertEquals(actualValue, expectedValue, message);
    }

    public void getTextEquals(WebElement element, String expectedValue, String message) {
        String actualValue = element.getText();
        instance.assertEquals(actualValue, expectedValue, message);
    }

    public void classAttrContains(WebElement element, String expectedValue, String message) {
        String actualValue = element.getAttribute("class");
        instance.assertTrue(actualValue.contains(expectedValue), message);
    }

    public void textContains(String actualValue, String expectedValue, String message) {
        instance.assertTrue(actualValue.contains(expectedValue), message);
    }

    public void assertAll() {
        instance.assertAll();
    }
}
