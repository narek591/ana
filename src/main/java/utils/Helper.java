package utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {
    public static String getParameterFromURI(String paramName, String URI) {
        String paramValue = "";
        String URIParameters = "";
        try {
            URIParameters = new URL(URI).getQuery();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String[] parametersArr = URIParameters.split("&");
        for (String parameter : parametersArr) {
            if (parameter.contains(paramName)) {
                paramValue = parameter.replace(paramName + "=", "");
            }
        }
        return paramValue;
    }

    public static String getPathFromURI() {
        String URI = DriverFactory.getDriver().getCurrentUrl();
        String path = "";
        try {
            path = new URL(URI).getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static String getTodayDate() {
        DateFormat sdf = new SimpleDateFormat("MMMM d, yyyy hh:mm");
        Date date = new Date();
        return sdf.format(date);
    }
}
