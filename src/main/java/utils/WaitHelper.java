package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHelper {
    private static Wait<WebDriver> wait;

    private static void setWait(int sec) {
        wait = new WebDriverWait(DriverFactory.getDriver(), sec);
    }

    private static String getXpathLocator(WebElement element) {
        return element.toString().split("-> xpath: ")[1].replaceFirst("(?s)(.*)\\]", "$1" + "");
    }

    public static void waitUntilIsClickable(WebElement element) {
        setWait(10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitUntilIsVisibility(WebElement element) {
        setWait(3);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForExist(WebElement element) {
        setWait(5);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(getXpathLocator(element))));
    }

    public static void waitForExist(String xpath) {
        setWait(5);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    }

    public static boolean waitUntilAttributeContains(WebElement element, String attribute, String value) {
        try {
            setWait(5);
            wait.until((ExpectedConditions.attributeContains(element, attribute, value)));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }
}
