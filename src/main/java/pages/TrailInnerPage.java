package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TrailInnerPage extends BasePage {
    public static String innerViewPATH(String name, String id) {
        String parsedName = name.toLowerCase().replaceAll(" ", "-");
        return "/hiking-trails/" + parsedName + "-" + id;
    }

    @FindBy(xpath = "//a[contains(@class,'view__actions-edit')]")
    public WebElement editBtn;

    @FindBy(xpath = "//div[@class='trail-title clearfix']/div[@class='breadcrumb-title']/h1")
    public WebElement titleText;
}