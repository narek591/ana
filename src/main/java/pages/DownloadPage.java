package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DownloadPage extends BasePage {
    public static final String PATH = "/trails/train";

    @FindBy(xpath = "//a[@data-value='last']")
    public WebElement train;
    @FindBy(xpath = "(//div[@id='trails']//ul[@class='trail-list']//div[@class='row'])[1]//h3/a")
    public WebElement trainTrail;
    @FindBy(xpath = "//a[@id='download-button']")
    public WebElement download;
    @FindBy(xpath = "//a[@href='#download-ge']")
    public WebElement googleEarth;
    @FindBy(xpath = "//input[@id='btn-download-ge']")
    public WebElement findDownload;
    @FindBy(xpath = "//*[@id='download-success']/p")
    public WebElement downloadCompleted;
}
