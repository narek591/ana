package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FollowPage extends BasePage {
    public static final String PATH = "/wikiloc/user.do?id=9095458";


    @FindBy(xpath = "//button[@id='follow-button']")
    public WebElement followButton;
}
