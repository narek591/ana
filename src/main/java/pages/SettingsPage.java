package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SettingsPage extends BasePage {
    public static final String PATH = "/wikiloc/showProfile.do";

    @FindBy(xpath = "//input[@id='prefNom']")
    public WebElement username;
    @FindBy(xpath = "//input[@id='prefEmail']")
    public WebElement emailAddress;
    @FindBy(xpath = "//input[@name='homePage']")
    public WebElement web;
    @FindBy(xpath = "//textarea[@id='prefAbout']")
    public WebElement aboutYou;
    @FindBy(xpath = "//button[@class='selector__container-mode-3__button']")
    public WebElement activities;
    @FindBy(xpath = "//div[@class='selector-icon-modal__content__body']//div[contains(text(),'Hiking')]")
    public WebElement hiking;
    @FindBy(xpath = "//div[@class='btn btn-success']")
    public WebElement select;
    @FindBy(xpath = "//*[@name='uom']")
    public WebElement unitsOfMeasurement;
    @FindBy(xpath = "//*[@value='metric']")
    public WebElement metric;
    @FindBy(xpath = "//input[@value='Save']")
    public WebElement save;

//    private static final String classCurrent = "current";
//    @FindBy(xpath = "//div[@id='content']//ul[contains(@class,'bubblenav')]//span[contains(text(),'Trails')]")
//    public WebElement trails;
//    @FindBy(xpath = "//div[@id='content']//ul[contains(@class,'bubblenav')]//span[contains(text(),'Your lists')]")
//    public WebElement yourLists;
//    @FindBy(xpath = "//div[@id='content']//ul[contains(@class,'bubblenav')]//span[contains(text(),'Follower')]")
//    public WebElement follower;
//    @FindBy(xpath = "//div[@id='content']//ul[contains(@class,'bubblenav')]//span[contains(text(),'Following')]")
//    public WebElement following;
//    @FindBy(xpath = "//div[@id='content']//ul[contains(@class,'bubblenav')]//span[contains(text(),'Trail Buddies')]")
//    public WebElement trailBuddies;
//    @FindBy(xpath = "//div[@id='content']//ul[contains(@class,'bubblenav')]//span[contains(text(),'Statistics')]")
//    public WebElement statistics;
}
