package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PromoteBusinessPage extends BasePage {
    public static final String PATH = "/wikiloc/poisedit.do";


    // Business creation
    @FindBy(xpath = "//input[@id='nom']")
    public WebElement nameOfYourBusiness;
    @FindBy(xpath = "//select[@id='poicat']")
    public WebElement options;
    @FindBy(xpath = "//*[@id='poicat']/optgroup[@label='Accommodations']/option[@value='1']")
    public WebElement type;
    @FindBy(xpath = "//input[@id='address']")
    public WebElement address;
    @FindBy(xpath = "//input[@id='email']")
    public WebElement email;
    @FindBy(xpath = "//input[@id='url']")
    public WebElement webSite;
    @FindBy(xpath = "//input[@id='reservationUrl']")
    public WebElement reservationUrl;
    @FindBy(xpath = "//input[@value='Continue']")
    public WebElement submit;

    //Publish Business
    @FindBy(xpath = "//input[@id='nif']")
    public WebElement taxidentificationnumber;
    @FindBy(xpath = "//input[@id='companyName']")
    public WebElement companyName;
    @FindBy(xpath = "//select[@id='country']")
    public WebElement countryOption;
    @FindBy(xpath = "//*[@id='country']/option[@value='Armenia']")
    public WebElement country;
    @FindBy(xpath = "//*[@id='address']")
    public WebElement businessaddress;
    @FindBy(xpath = "//*[@id='postalCode']")
    public WebElement postalCode;
    @FindBy(xpath = "//*[@class='selected-dial-code']")
    public WebElement countryList;
    @FindBy(xpath = "//*[@data-dial-code='374']")
    public WebElement countryCode;
    @FindBy(xpath = "//input[@id='phonetmp']")
    public WebElement number;
    @FindBy(xpath = "//input[@id='firstName']")
    public WebElement firstName;
    @FindBy(xpath = "//input[@id='lastName']")
    public WebElement lastName;
    @FindBy(xpath = "//input[@id='email']")
    public WebElement personalEmail;
    @FindBy(xpath = "//input[@id='bc']")
    public WebElement submitbusiness;


    //credit card info
    @FindBy(xpath = "//input[@id='inputCard']")
    public WebElement cardNumber;
    @FindBy(xpath = "//input[@id='cad1']")
    public WebElement expDate1;
    @FindBy(xpath = "//input[@id='cad2']")
    public WebElement expDate2;
    @FindBy(xpath = "//input[@id='codseg']")
    public WebElement  securityCode;
    @FindBy(xpath = "//button[@id='divImgAceptar']")
    public WebElement accept;

}
