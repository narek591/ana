package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YourListPage extends BasePage {
    public static final String PATH = "/wikiloc/user.do?id=29629";

    @FindBy(xpath = "//a[@title='Ruta por Moncalvillo trail - Pedrezuela, Madrid (España)']")
    public WebElement road;
    @FindBy(xpath ="//a[@title='Bonita Ruta por Moncalvillo trail - Pedrezuela, Madrid (España)']")
    public WebElement road1;
    @FindBy(xpath = "//div[@class='view__actions-savetolist wlbutton wlbutton--xs wlbutton--sec-green']")
    public WebElement saveToList;

    @FindBy(xpath = "//div[@class='add-to-fav-item__name-value'][text()='Favorites']")
    public WebElement favorites;
    @FindBy(xpath = "//div[@class='add-to-fav-item__name-value'][text()='Want to go']")
    public WebElement wantToGo;
    @FindBy(xpath = "//button[@aria-label='Close']")
    public WebElement close;
    @FindBy(xpath = "//div[@class='clap-img']")
    public WebElement clap;

    @FindBy(xpath = "//button[@id='view-action-comment']")
    public WebElement comment;
    @FindBy(xpath = "//input[@type='submit']")
    public WebElement sendComment;


}
