package pages.upload;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class UploadPage extends BasePage {
    public static final String PATH = "/wikiloc/upload.do";

    @FindBy(xpath = "//input[@id='gpsDataFile']")
    public WebElement fileInput;
    @FindBy(xpath = "//form[@id='addGpsData']//input[@id='submit-btn']")
    public WebElement addGpsContinueBtn;

    @FindBy(xpath = "//form[@id='addGpsData']//label")
    public WebElement uploadedFileTitle;

    @FindBy(xpath = "//a[@id='dz-modal-trigger']")
    public WebElement uploadExternalBtn;
    @FindBy(xpath = "//input[@id='urlLarge']")
    public WebElement uploadExternalInput;
    @FindBy(xpath = "//button[@id='btn-upload-external']")
    public WebElement uploadExternalSave;

    // General Elements
    @FindBy(xpath = "//input[@id='nom']")
    public WebElement fileName;
    @FindBy(xpath = "//textarea[@id='descripcio']")
    public WebElement description;

    // Difficulty Levels
    @FindBy(xpath = "//button[@data-skill='1']")
    public WebElement skillEasy;
    @FindBy(xpath = "//button[@data-skill='2']")
    public WebElement skillModerate;
    @FindBy(xpath = "//button[@data-skill='3']")
    public WebElement skillDifficult;
    @FindBy(xpath = "//button[@data-skill='4']")
    public WebElement skillVeryDifficult;
    @FindBy(xpath = "//button[@data-skill='5']")
    public WebElement skillExpertsOnly;

    @FindBy(xpath = "//button[@class='selector__container-mode-2__button']")
    public WebElement changeActivityTypeBtn;

    @FindBy(xpath = "//form[@id='edit']/..//input[@id='submit-btn']")
    public WebElement finalContinueBtn;

    // Privacy Elements
    @FindBy(xpath = "//button[@class='privacy-selector__btn-privacy']")
    public WebElement privacyBtn;
    @FindBy(xpath = "//input[@id='private-selector']")
    public WebElement privateOption;
    @FindBy(xpath = "//button[@class='btn btn-success privacy-selector-modal__container__content__accept']")
    public WebElement privateModalSubmitBtn;
    @FindBy(xpath = "//button[@class='privacy-selector__btn-privacy']")
    public WebElement privacyStatus;


    @FindBy(xpath = "//form[@id='form-companions']//div[@id='single']/a")
    public WebElement doneAlone;

    @FindBy(xpath = "//div[@class='selector-icon-button__text'][contains(text(),'Barefoot')]")
    public WebElement categoryBarefoot;

    public String getPreviewImageXpath(String text){
        return "(//img[contains(@alt, "+ text +")]/../../parent::div[@data-id])[1]";
    }
}