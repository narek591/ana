package pages.upload;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.TrailInnerPage;

public class UploadPageDelete extends TrailInnerPage {
    @FindBy(xpath = "//button[@id='delete-trail']")
    public WebElement deleteBtn;

    @FindBy(xpath = "//h1")
    public WebElement error404text;

}