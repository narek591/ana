package pages.upload;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UploadPageEdit extends UploadPage {
    @FindBy(xpath = "//button[contains(@class,'privacy-selector-modal__container__content__accept')]")
    public WebElement privacyModalToPrivateConfirmBtn;

    @FindBy(xpath = "//input[@id='tags']")
    public WebElement tags;
    @FindBy(xpath = "//input[@id='url']")
    public WebElement externalURL;
}
