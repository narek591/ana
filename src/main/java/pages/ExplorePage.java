package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExplorePage extends BasePage {
    public static final String PATH = "/wikiloc/map.do";

    @FindBy(css = ".filters > div:nth-of-type(1) > .filters__item.filters__item--clear")
    public WebElement activities;
    @FindBy(xpath = "//div[@class='selector-icon-button__text'][contains(text(),'Hiking')]")
    public WebElement hiking;
    @FindBy(xpath = "//div[@class='main__filters']//button[@class='btn btn-success shortcut-filter__apply']")
    public WebElement applyActivitiesFilterBtn;


    @FindBy(css = "div:nth-of-type(2) > .filters__item.filters__item--clear")
    public WebElement distance;
    @FindBy(className = "shortcut-filter__inner")
    public WebElement distanceInner;
    @FindBy(xpath = "//div[@class='filter-distance']/div[@class='slider']/div[@class='slider__line']/div/div[@class='noUi-base']/div[2]/div[@role='slider']/div[@class='noUi-touch-area']")
    public WebElement lineFirstBtn;
    @FindBy(xpath = "//div[@class='filter-distance']/div[@class='slider']/div[@class='slider__line']/div/div[@class='noUi-base']/div[3]/div[@role='slider']/div[@class='noUi-touch-area']")
    public WebElement lineSecondBtn;
    @FindBy(xpath = "//div[@class='filter-distance']/div[@class='slider']/div[@class='slider__values']/div[@class='slider__values-min']")
    public WebElement minValue;
    @FindBy(xpath = "//div[@class='filter-distance']/div[@class='slider']/div[@class='slider__values']/div[@class='slider__values-max']")
    public WebElement maxValue;
    @FindBy(xpath = "//div[@class='main__filters']//button[@class='btn btn-success shortcut-filter__apply']")
    public WebElement applyFilterBtn;


    @FindBy(css = ".filters > .filters__item.filters__item--clear")
    public WebElement moreFilters;
    @FindBy(className = "filters__close")
    public WebElement closeBtn;
    @FindBy(className = "filters__footer-remove")
    public WebElement clearFiltersBtn;
    @FindBy(className = "filters__row filters__row--title")
    public WebElement filters;
    @FindBy(className = "filters__row filters__row--responsive-column")
    public WebElement elevation;
    @FindBy(xpath = "//div[@class='filters__row']/div[@class='filters__row-body']/input[@type='checkbox']")
    public WebElement toggleBtn;


}
