package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utils.DriverFactory;

public class BasePage {
    public static final String BASE_URL = "https://www.wikiloc.com";
    public static final String ABOUT_US_PATH = BASE_URL + "/wikiloc/about-us.do";

    public BasePage() {
        PageFactory.initElements(DriverFactory.getDriver(), this);
    }

    public static void sleep(int sec) {
        try {
            Thread.sleep(sec * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void cleanAndSendKeys(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }
}
